import React from 'react';

class MarkTable extends React.Component
{
    render(){
        return (
            <table className="ui celled table">
                <thead>
                    <tr>
                        <th>Universiteto pavadinimas</th>
                        <th>Vardas pavardė</th>
                        {
                            //subjects headers
                            this.props.subjects.map(subject=>{
                                return <th key={subject.subjectCode}>{subject.subjectName}</th>
                            })
                        }
                    </tr>
                </thead>
                <tbody>
                    {
                        Object.entries(this.props.marks).map(([studentId]) => 
                        {
                            return (
                                <tr key={studentId}>
                                    <td>{this.props.marks[studentId]['university']}</td>
                                    <td>{this.props.marks[studentId]['fullName']}</td>
                                    {
                                        
                                        this.props.subjects.map(subject=>
                                        {
                                            return (
                                                <td key={"averageMark"+studentId+subject.subjectCode}>
                                                {
                                                    //search student averages by subject code
                                                    this.props.marks[studentId]['subjectAverages'][subject.subjectCode] ?
                                                    this.props.marks[studentId]['subjectAverages'][subject.subjectCode] :
                                                    "Nėra pažymių"
                                                }
                                                </td>
                                            );

                                        })
                                    }
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        );
    }
    
}

export default MarkTable;