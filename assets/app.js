import './styles/app.css';

import React from 'react';
import ReactDOM from 'react-dom';
import MarkTable from './components/MarkTable'
import axios from 'axios';

class App extends React.Component
{
    constructor() {
        super();
        this.state = { 'subjects': [],'marks':null };
    }

    componentDidMount() {
        axios.all([axios.get(`/api/subject`),
           axios.post(`/api/marks/average`)])
     .then(axios.spread((subjects, marks) => {
        
        this.setState({'subjects':subjects.data,'marks':marks.data})
         
     }))
     
    }

    renderContent  ()  {
        if(this.state.subjects.length>0 && this.state.marks)
        {
            return <MarkTable subjects={this.state.subjects} marks={this.state.marks}/>;
        }
        return <div>Loading...</div>;
    }


    render(){
       
        return (
            <div>
                {this.renderContent()}
            </div>
        )
    }
}
ReactDOM.render(<App/>,document.getElementById('root'))