<?php

namespace App\Service;

use App\Entity\Mark;
use App\Entity\Subject;
use App\Entity\University;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DatabaseService extends AbstractController
{
    /**
     * Function returns an array of all students with its subject average marks
     *
     * @return array
     */
    public function getStudentAverageMarks(): array
    {
        $manager=$this->getDoctrine()->getManager();
        
        $qb=$manager->createQueryBuilder();
        
        $qb->select('u.name university','s.id studentId','concat(s.first_name,\' \',s.last_name) studentFullName','sb.code subjectCode','avg(m.mark) averageMark')
        ->from('App:Student','s')
        ->join(Mark::class,'m',Join::WITH,'m.student=s.id')
        ->join(Subject::class,'sb',Join::WITH,'sb.id=m.subject')
        ->join(University::class,'u',Join::WITH,'u.id=s.university')
        ->addGroupBy('sb.code')
        ->addGroupBy('s.id');

        $results=$qb->getQuery()->getArrayResult();

        $students=[];

        foreach ($results as $result) {
            if (!array_key_exists($result['studentId'],$students)) {
                $students[$result['studentId']]=[
                    'university'=>$result['university'],
                    'fullName'=>$result['studentFullName'],
                ];
            }
            $students[$result['studentId']]['subjectAverages'][$result['subjectCode']]=number_format(round($result['averageMark'],1),1);
        }
        
        
        
        
        return $students;
    }
    /**
     * Function returns an array of all subjects
     *
     * @return array
     */
    public function getAllSubjects(): array
    {
        $manager=$this->getDoctrine()->getManager();
        
        $qb=$manager->createQueryBuilder();

        $qb->select('sb.code subjectCode','sb.name subjectName')
        ->from('App:Subject','sb');

        return $qb->getQuery()->getArrayResult();
    }
}