<?php

namespace App\Controller;

use App\Service\DatabaseService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MarksController extends AbstractController
{
    /**
     * Controller function for fetching post method with 'api/marks/average' url path 
     * and return JSON with averages students marks with subjects array
     * using $databaseService function getStudentAverageMarks
     * 
     * @Route("/api/marks/average", methods={"post"})
     * @param DatabaseService $databaseService service class for communicating with database
     * @return Response JSON response
     */
    public function index(DatabaseService $databaseService): Response
    {
        $results=$databaseService->getStudentAverageMarks();

        return new Response(json_encode($results));
    }
}
