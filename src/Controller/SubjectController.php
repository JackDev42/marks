<?php

namespace App\Controller;

use App\Service\DatabaseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubjectController extends AbstractController
{
    /**
     * Returns all subjects
     *
     * @param DatabaseService $databaseService
     * @Route("/api/subject", name="subject",methods={"get"})
     * @return Response
     */
    public function index(DatabaseService $databaseService): Response
    {
        $subjects=$databaseService->getAllSubjects();

        return new Response(json_encode($subjects));
        
    }
}
