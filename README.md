# Marks  
To run this project you need to  

1. specify database type and edit .env file  
-  `login`  
-  `password`  
-  `url path`  
-  `port`  
-  `name`  
1.  run `php bin/console doctrine:database:create` command in CMD to create database connection  
1.  run `php bin/console doctrine:migrations:migrate` command in CMD to migrate entities to database  
1.  run `php bin/console doctrine:database:import ./path_to_your_sql_file/file_name.sql` to import data to database   


Project runs at url path: `server_address/home`  
